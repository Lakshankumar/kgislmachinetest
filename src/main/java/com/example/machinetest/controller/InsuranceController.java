package com.example.machinetest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.machinetest.Utils.Constants;
import com.example.machinetest.entity.Branch;
import com.example.machinetest.entity.BranchManager;
import com.example.machinetest.entity.PolicyHolder;
import com.example.machinetest.service.BranchManagerService;
import com.example.machinetest.service.BranchService;
import com.example.machinetest.service.PolicyHolderService;

@SuppressWarnings({ "unchecked", "rawtypes" })
@Controller
public class InsuranceController {

	@Autowired
	private BranchService branchRepo;

	@Autowired
	private BranchManagerService branchManagerRepo;

	@Autowired
	private PolicyHolderService policyHolderRepo;

	/*
	 * @RequestMapping(value = "/managebranch", method = RequestMethod.POST) public
	 * String createBranch(Model model,@ModelAttribute("branch") @Valid @RequestBody
	 * Branch branch) { Branch bran=branchRepo.findLast();
	 * if(bran!=null&&bran.getId()>0) { String
	 * token[]=bran.getBranchToken().split("-"); int
	 * tokenIncre=Integer.parseInt(token[1]);
	 * branch.setBranchToken("Branch-"+(tokenIncre+1)); }else {
	 * branch.setBranchToken("Branch-1"); } branchRepo.save(branch); return
	 * "redirect:/home"; }
	 */

	@RequestMapping(value = { "/managebranch" }, method = RequestMethod.POST)
	public ModelAndView createBranch(@Valid Branch branch, BindingResult bindingResult) {
		ModelAndView model = new ModelAndView();

		if (bindingResult.hasErrors()) {
			model.setViewName("managebranch");
			model.addObject("branchList", branchRepo.findAll());
		} else {

			System.out.println("Val branch:"+branch.getId() +" token:"+branch.getBranchToken());
			if (branch.getBranchToken() == null) {
				Branch bran = branchRepo.findLast();
				if (bran != null && bran.getId() > 0) {
					String token[] = bran.getBranchToken().split("-");
					int tokenIncre = Integer.parseInt(token[1]);
					branch.setBranchToken("Branch-" + (tokenIncre + 1));
				} else {
					branch.setBranchToken("Branch-1");
				}
				model.addObject("msg", "Branch has been Registered successfully!");
			} else {
				model.addObject("msg", "Branch has been Updated successfully!");
			}
			branchRepo.save(branch);
			model.addObject("branchList", branchRepo.findAll());
			model.addObject("branch", new Branch());
			model.setViewName("managebranch");
		}

		return model;
	}

	@RequestMapping(value = { "/managebranchmanager" }, method = RequestMethod.POST)
	public ModelAndView createBranchManager(@Valid BranchManager branchMgr, BindingResult bindingResult) {
		ModelAndView model = new ModelAndView();

		List<Branch> branList = branchRepo.findAll();
		BranchManager form = new BranchManager();
		model.addObject("branchlist", branList);
		model.addObject("branchManager", form);

		if (bindingResult.hasErrors()) {
			model.setViewName("managebranchmanager");
		} else {

			if (branchMgr.getManagerToken() == null) {
				BranchManager bran = branchManagerRepo.findLast();
				if (bran != null && bran.getId() > 0) {
					String token[] = bran.getManagerToken().split("-");
					int tokenIncre = Integer.parseInt(token[1]);
					branchMgr.setManagerToken("BranchMgr-" + (tokenIncre + 1));
				} else {
					branchMgr.setManagerToken("BranchMgr-1");
				}
				branchMgr.setBranchId(branchRepo.findBranchByToken(branchMgr.getBranchMap()));
				model.addObject("msg", "Branch Manager has been registered successfully!");
			} else {
				branchMgr.setBranchId(branchRepo.findBranchByToken(branchMgr.getBranchMap()));
				model.addObject("msg", "Branch Manager has been updated successfully!");
			}
			branchMgr.setManagerPassword(branchMgr.getManagerToken());
			branchManagerRepo.save(branchMgr);
			model.addObject("branchManager", new BranchManager());
			model.addObject("branchManagerList", branchManagerRepo.findAll());
			model.setViewName("managebranchmanager");
		}

		return model;
	}

	@RequestMapping(value = { "/managepolicyholder" }, method = RequestMethod.POST)
	public ModelAndView createPolicyHolder(@Valid PolicyHolder policyHolder, BindingResult bindingResult) {
		ModelAndView model = new ModelAndView();

		List<BranchManager> branMgrList = branchManagerRepo.findAll();
		PolicyHolder policy = new PolicyHolder();
		model.addObject("policyHolder", policy);
		model.addObject("branmgrlist", branMgrList);

		if (bindingResult.hasErrors()) {
			model.setViewName("managepolicyholder");
		} else {
			System.out.println("Process complete");
			if (policyHolder.getHolderToken() == null) {
				PolicyHolder policyhol = policyHolderRepo.findLast();
				if (policyhol != null && policyhol.getId() > 0) {
					String token[] = policyhol.getHolderToken().split("-");
					int tokenIncre = Integer.parseInt(token[1]);
					policyHolder.setHolderToken("PolicyHol-" + (tokenIncre + 1));
				} else {
					policyHolder.setHolderToken("PolicyHol-1");
				}
				policyHolder.setBranchMgnrId(
						branchManagerRepo.findBranchManagerByToken(policyHolder.getBranchManagerMap()));
				// policyhol.setBranchMgnrId(poli);
				// branchMgr.setBranchId(branchRepo.findBranchByToken(branchMgr.getBranchMap()));
				model.addObject("msg", "Policy Holder has been registered successfully!");
			} else {
				policyHolder.setBranchMgnrId(
						branchManagerRepo.findBranchManagerByToken(policyHolder.getBranchManagerMap()));
				model.addObject("msg", "Policy Holder has been updated successfully!");
			}
			policyHolderRepo.save(policyHolder);
			model.addObject("policyHolderList", policyHolderRepo.findAll());
			model.addObject("policyHolder", policy);
			model.setViewName("managepolicyholder");
		}

		return model;
	}

	@PostMapping(Constants.SAVEBRANCHMANAGER)
	public ResponseEntity<Object> createBrnchManager(@Valid @RequestBody BranchManager branchMg,
			@PathVariable("branchId") int branchId) {
		Branch branch = branchRepo.findById(branchId);
		if (branch != null && branch.getId() > 0) {
			BranchManager bran = branchManagerRepo.findLast();
			if (bran != null && bran.getId() > 0) {
				String token[] = bran.getManagerToken().split("-");
				int tokenIncre = Integer.parseInt(token[1]);
				branchMg.setManagerToken("BranchMgr-" + (tokenIncre + 1));
			} else {
				branchMg.setManagerToken("BranchMgr-1");
			}
			branchMg.setBranchId(branch);
			branchManagerRepo.save(branchMg);
			return new ResponseEntity(branchMg, HttpStatus.OK);
		} else {
			return new ResponseEntity("Branch not matched", HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value = Constants.SAVEPOLICYHOLDER)
	public ResponseEntity<Object> createPolicy(@Valid @RequestBody PolicyHolder policy,
			@PathVariable int branchMgnrId) {
		BranchManager branchMgr = branchManagerRepo.findById(branchMgnrId);
		if (branchMgr != null && branchMgr.getId() > 0) {

			PolicyHolder poli = policyHolderRepo.findLast();
			if (poli != null && poli.getId() > 0) {
				String token[] = poli.getHolderToken().split("-");
				int tokenIncre = Integer.parseInt(token[1]);
				policy.setHolderToken("PolicyHol-" + (tokenIncre + 1));
			} else {
				policy.setHolderToken("PolicyHol-1");
			}
			policy.setBranchMgnrId(branchMgr);
			policyHolderRepo.save(policy);
			return new ResponseEntity(policy, HttpStatus.OK);
		} else {
			return new ResponseEntity("Branch not matched", HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value = Constants.UPDATEBRANCH)
	public ResponseEntity<Branch> updateBranch(@Valid @RequestBody Branch branch) {
		Branch temp = branchRepo.findById(branch.getId());
		temp.setBranchAddres(branch.getBranchAddres());
		temp.setBranchName(branch.getBranchName());
		temp.setBranchPhoneNumber(branch.getBranchPhoneNumber());
		branchRepo.update(temp);
		return new ResponseEntity<Branch>(branch, HttpStatus.OK);
	}

	@PostMapping(value = Constants.UPDATEBRANCHMANAGER)
	public ResponseEntity<BranchManager> updateBrnchManager(@Valid @RequestBody BranchManager branchMg) {
		BranchManager temp = branchManagerRepo.findById(branchMg.getId());
		temp.setManagerAddress(branchMg.getManagerAddress());
		temp.setManagerName(branchMg.getManagerName());
		temp.setManagerPhoneNumber(branchMg.getManagerPhoneNumber());
		temp.setManagerPassword(branchMg.getManagerPassword());
		branchManagerRepo.update(temp);
		return new ResponseEntity<BranchManager>(branchMg, HttpStatus.OK);
	}

	@PostMapping(value = Constants.UPDATEPOLICYHOLDER)
	public ResponseEntity<PolicyHolder> updatePolicy(@Valid @RequestBody PolicyHolder policy) {
		PolicyHolder holder = policyHolderRepo.findById(policy.getId());
		holder.setHolderAddress(policy.getHolderAddress());
		holder.setHolderName(policy.getHolderName());
		holder.setHolderPhonenumber(policy.getHolderPhonenumber());
		holder.setHolderPolicyDetails(policy.getHolderPolicyDetails());
		policyHolderRepo.update(holder);
		return new ResponseEntity<PolicyHolder>(policy, HttpStatus.OK);
	}

	@DeleteMapping(value = Constants.DELETEBRANCH)
	public void deleteBranch(@PathVariable int id) {
		branchRepo.delete(id);
	}

	@DeleteMapping(value = Constants.DELETEBRANCHMANAGER)
	public void deleteBranchManager(@PathVariable int id) {
		branchRepo.delete(id);
	}

	@DeleteMapping(value = Constants.DELETEPOLICYHOLDER)
	public void deletePolicyHolder(@PathVariable int id) {
		branchRepo.delete(id);
	}

	@GetMapping(value = Constants.FINDALLBRANCH)
	public @ResponseBody List<Branch> findAllBranch() {
		return branchRepo.findAll();
	}

	@GetMapping(value = Constants.FINDALLPOLICYHOLDER)
	public @ResponseBody List<PolicyHolder> findAllPolicyHolder() {
		return policyHolderRepo.findAll();
	}

	@GetMapping(value = Constants.FINDALLBRANCHMANAGER)
	public @ResponseBody List<BranchManager> findAllBranchManager() {
		return branchManagerRepo.findAll();
	}

	@GetMapping(value = Constants.FINDBRANCH)
	public @ResponseBody Branch getBranch(@PathVariable int id) {
		return branchRepo.findById(id);
	}

	@GetMapping(value = Constants.FINDBRANCHMANAGER)
	public @ResponseBody Branch getBranchManager(@PathVariable int id) {
		return branchRepo.findById(id);
	}

	@GetMapping(value = Constants.FINDPOLICYHOLDER)
	public @ResponseBody Branch getPolicyHolder(@PathVariable int id) {
		return branchRepo.findById(id);
	}

	@GetMapping("/login")
	public String login() {
		return "/login";
	}

	@GetMapping("/403")
	public String error403() {
		return "/error/403";
	}

	@GetMapping("/")
	public String home1() {
		return "/home";
	}

	@GetMapping("/home")
	public String home() {
		return "/home";
	}

	@GetMapping("/managebranch")
	public String manageBranch(Model model) {
		Branch form = new Branch();
		model.addAttribute("branchList", branchRepo.findAll());
		model.addAttribute("branch", form);
		return "managebranch";
	}

	@GetMapping("/managebranchmanager")
	public String manageBranchManager(Model model) {
		List<Branch> branList = branchRepo.findAll();
		BranchManager form = new BranchManager();

		model.addAttribute("branchlist", branList);
		model.addAttribute("branchManager", form);
		model.addAttribute("branchManagerList", branchManagerRepo.findAll());
		if (branList != null && !branList.isEmpty()) {
			return "managebranchmanager";
		} else {
			return "home";
		}
	}

	@GetMapping("/managepolicyholder")
	public String managePolicyHolder(Model model) {
		List<BranchManager> branManagerList = branchManagerRepo.findAll();
		PolicyHolder holder = new PolicyHolder();
		model.addAttribute("branmgrlist", branManagerList);
		model.addAttribute("policyHolder", holder);
		model.addAttribute("policyHolderList", policyHolderRepo.findAll());
		if (branManagerList != null && !branManagerList.isEmpty()) {
			return "managepolicyholder";
		} else {
			return "home";
		}
	}
	
	@GetMapping("/managebranch/{id}")
	public String editBranch(@PathVariable("id") Integer id, Model model ) {
		model.addAttribute("branch", branchRepo.findById(id));
		model.addAttribute("branchList", branchRepo.findAll());
	    return  "managebranch";
	}
	
	@GetMapping("/managebranchmanager/{id}")
	public String editBranchManager(@PathVariable("id") Integer id, Model model ) {
		BranchManager manager=branchManagerRepo.findById(id);
		manager.setBranchMap(manager.getBranchId().getBranchToken());
		model.addAttribute("branchManager", manager);
		model.addAttribute("branchlist", branchRepo.findAll());
		model.addAttribute("branchManagerList", branchManagerRepo.findAll());
	    return  "managebranchmanager";
	}
	
	@GetMapping("/managepolicyholder/{id}")
	public String editPolicyHolder(@PathVariable("id") Integer id, Model model ) {
		PolicyHolder policy=policyHolderRepo.findById(id);
		policy.setBranchManagerMap(policy.getBranchMgnrId().getManagerToken());
		model.addAttribute("policyHolder", policy);
		model.addAttribute("policyHolderList", policyHolderRepo.findAll());
		model.addAttribute("branmgrlist", branchManagerRepo.findAll());
	    return  "managepolicyholder";
	}

}
