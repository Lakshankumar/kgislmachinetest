package com.example.machinetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KgislmachinetestApplication {

	public static void main(String[] args) {
		SpringApplication.run(KgislmachinetestApplication.class, args);
	}

}
