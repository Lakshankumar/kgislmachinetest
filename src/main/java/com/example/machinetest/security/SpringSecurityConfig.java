package com.example.machinetest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.example.machinetest.Utils.Constants;
import com.example.machinetest.Utils.USER_ROLES;


@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	

	@Autowired
    private AccessDeniedHandler accessDeniedHandler;
	
	
	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.inMemoryAuthentication()
        .withUser("user").password("password").roles(USER_ROLES.BRANCHMANAGER.name())
        .and()
        .withUser("admin").password("password").roles(USER_ROLES.ADMIN.name());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

      
    	http
        .authorizeRequests()
        .antMatchers("/","/home").permitAll()
        .antMatchers(HttpMethod.GET, Constants.FINDALLPOLICYHOLDER,"/findPolicyHolderById/**").hasRole(USER_ROLES.BRANCHMANAGER.name())
        .antMatchers(HttpMethod.GET, Constants.FINDALLBRANCH,Constants.FINDALLBRANCHMANAGER,Constants.FINDALLPOLICYHOLDER,"/findBranchById/**","/findBranchManagerById/**","/findPolicyHolderById/**").hasRole(USER_ROLES.ADMIN.name())
        .antMatchers(HttpMethod.POST, Constants.SAVEBRANCH,Constants.UPDATEBRANCH,Constants.UPDATEBRANCHMANAGER,Constants.UPDATEPOLICYHOLDER,"/savebranchmgr/**","/savepolicyholder/**").hasRole(USER_ROLES.ADMIN.name())
        .antMatchers(HttpMethod.POST, Constants.UPDATEPOLICYHOLDER,"/savepolicyholder/**").hasRole(USER_ROLES.BRANCHMANAGER.name())
        .antMatchers(HttpMethod.DELETE, "/deletePolicyHolder/**").hasRole(USER_ROLES.BRANCHMANAGER.name())
        .antMatchers(HttpMethod.DELETE, "/deleteBranch/**","/deleteBranchManager/**","/deletePolicyHolder/**").hasRole(USER_ROLES.ADMIN.name())
        .antMatchers(HttpMethod.GET, "/managebranch/**","/managebranchmanager/**").hasRole(USER_ROLES.ADMIN.name())
        .antMatchers(HttpMethod.GET, "/managepolicyholder/**").hasRole(USER_ROLES.BRANCHMANAGER.name())
        .antMatchers(HttpMethod.POST, "/managebranch/**","/managebranchmanager/**").hasRole(USER_ROLES.ADMIN.name())
        .antMatchers(HttpMethod.POST, "/managepolicyholder/**").hasRole(USER_ROLES.BRANCHMANAGER.name())
        .anyRequest().authenticated()
        .and()
        .formLogin()
			.loginPage("/login")
			.permitAll()
			.and()
        .logout()
			.permitAll()
			.and()
        .exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    	
    	http.csrf().disable();
    }

}
