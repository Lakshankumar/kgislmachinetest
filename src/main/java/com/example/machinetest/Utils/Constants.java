package com.example.machinetest.Utils;

public class Constants {
	
	public static final String SAVEBRANCH="/savebranch";
	
	public static final String SAVEBRANCHMANAGER="/savebranchmgr/{branchId}";
	
	public static final String SAVEPOLICYHOLDER="/savepolicyholder/{branchMgnrId}";
	
	public static final String UPDATEBRANCH="/updatebranch";
	
	public static final String UPDATEBRANCHMANAGER="/updatebranchmgr";
	
	public static final String UPDATEPOLICYHOLDER="/updatepolicyholder";
	
	public static final String DELETEBRANCH="/deleteBranch/{id}";
	
	public static final String DELETEBRANCHMANAGER="/deleteBranchManager/{id}";
	
	public static final String DELETEPOLICYHOLDER="/deletePolicyHolder/{id}";
	
	public static final String FINDALLBRANCH="/findAllBranch";
	
	public static final String FINDALLBRANCHMANAGER="/findAllPolicyHolder";
	
	public static final String FINDALLPOLICYHOLDER="/findAllBranchManager";
	
	public static final String FINDBRANCH="/findBranchById/{id}";
	
	public static final String FINDBRANCHMANAGER="/findBranchManagerById/{id}";
	
	public static final String FINDPOLICYHOLDER="/findPolicyHolderById/{id}";
	

}
