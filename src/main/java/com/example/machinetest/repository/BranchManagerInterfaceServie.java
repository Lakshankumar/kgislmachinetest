package com.example.machinetest.repository;

import java.util.List;

import com.example.machinetest.entity.BranchManager;

public interface BranchManagerInterfaceServie {

	BranchManager findById(int id);
	BranchManager save(BranchManager brmanager);
	BranchManager update(BranchManager brmanager);
	void delete(int id);
    List<BranchManager> findAll();
    BranchManager findLast();
    BranchManager findBranchManagerByToken(String token);
}
