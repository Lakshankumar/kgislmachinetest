package com.example.machinetest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.machinetest.entity.BranchManager;

@Repository
public interface BranchManagerRepo extends JpaRepository<BranchManager, Integer>{

	@Query(value = "select p from BranchManager p where p.managerToken=:token")
	List<BranchManager> findBranchManagerById(@Param("token") String token);
}
