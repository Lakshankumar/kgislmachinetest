package com.example.machinetest.repository;

import java.util.List;

import com.example.machinetest.entity.PolicyHolder;

public interface PolicyHolderInterfaceService {

	PolicyHolder findById(int id);
	PolicyHolder save(PolicyHolder policy);
	PolicyHolder update(PolicyHolder policy);
	void delete(int id);
    List<PolicyHolder> findAll();
    PolicyHolder findLast();
}
