package com.example.machinetest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.machinetest.entity.PolicyHolder;

@Repository
public interface PolicyHolderRepo extends JpaRepository<PolicyHolder, Integer>{

}
