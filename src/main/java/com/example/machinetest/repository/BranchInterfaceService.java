package com.example.machinetest.repository;

import java.util.List;

import com.example.machinetest.entity.Branch;

public interface BranchInterfaceService {
	
	Branch findById(int id);
    Branch save(Branch branch);
    Branch update(Branch branch);
    void delete(int id);
    List<Branch> findAll();
    Branch findLast();
    Branch findBranchByToken(String token);
}
