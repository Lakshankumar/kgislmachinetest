package com.example.machinetest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.machinetest.entity.PolicyHolder;
import com.example.machinetest.exceptionhandle.NoDataFoundException;
import com.example.machinetest.exceptionhandle.PolicyHolderNotFound;
import com.example.machinetest.repository.PolicyHolderInterfaceService;
import com.example.machinetest.repository.PolicyHolderRepo;

@Service
public class PolicyHolderService implements PolicyHolderInterfaceService{
	
	@Autowired
	private PolicyHolderRepo policyRepo;

	@Override
	public PolicyHolder findById(int id) {
		PolicyHolder pohol=policyRepo.findOne(id);
		if(pohol==null) {
			throw new PolicyHolderNotFound(id);
		}else {
			return pohol;
		}
	}

	@Override
	public PolicyHolder save(PolicyHolder policy) {
		return policyRepo.save(policy);
	}

	@Override
	public PolicyHolder update(PolicyHolder policy) {
		return policyRepo.save(policy);
	}

	@Override
	public void delete(int id) {
		
		PolicyHolder pohol=policyRepo.findOne(id);
		if(pohol==null) {
			throw new PolicyHolderNotFound(id);
		}else {
			if(pohol!=null&&pohol.getId()>0) {
				policyRepo.delete(pohol.getId());
			}
		}
	}

	@Override
	public List<PolicyHolder> findAll() {
		
		List<PolicyHolder> policyList = policyRepo.findAll();

        if (policyList.isEmpty()) {

            throw new NoDataFoundException();
        }

        return policyList;
	}

	@Override
	public PolicyHolder findLast() {
		List<PolicyHolder> policyHolderList = policyRepo.findAll();
		if(policyHolderList!=null&&!policyHolderList.isEmpty()) {
			return policyHolderList.get(policyHolderList.size()-1);
		}else {
			return null;
		}
	}

}
