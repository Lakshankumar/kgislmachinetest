package com.example.machinetest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.machinetest.entity.Branch;
import com.example.machinetest.exceptionhandle.BranchNotFound;
import com.example.machinetest.exceptionhandle.NoDataFoundException;
import com.example.machinetest.repository.BranchInterfaceService;
import com.example.machinetest.repository.BranchRepo;

@Service
public class BranchService implements BranchInterfaceService{
	
	@Autowired
	private BranchRepo branchRepo;

	@Override
	public Branch findById(int id) {
		Branch br=branchRepo.findOne(id);
		if(br==null) {
			throw new BranchNotFound(id);
		}else {
			return br;
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public Branch save(Branch branch) {
		return branchRepo.save(branch);
	}

	@Override
	public Branch update(Branch branch) {
		return branchRepo.save(branch);
	}

	@Override
	public void delete(int id) {
		
		Branch br=branchRepo.findOne(id);
		if(br==null) {
			throw new BranchNotFound(id);
		}else {
			if(br!=null&&br.getId()>0) {
				branchRepo.delete(br.getId());
			}
		}
	}

	@Override
	public List<Branch> findAll() {
		List<Branch> branchList = branchRepo.findAll();

        if (branchList.isEmpty()) {

            throw new NoDataFoundException();
        }

        return branchList;
	}

	@Override
	public Branch findLast() {
		List<Branch> branchList = branchRepo.findAll();
		if(branchList!=null&&!branchList.isEmpty()) {
			return branchList.get(branchList.size()-1);
		}else {
			return null;
		}
	}

	@Override
	public Branch findBranchByToken(String token) {
		List<Branch> branchList=branchRepo.findBranchById(token);
		if(branchList!=null&&!branchList.isEmpty()){
			return branchList.get(0);
		}else {
			return null;
		}
	}

}
