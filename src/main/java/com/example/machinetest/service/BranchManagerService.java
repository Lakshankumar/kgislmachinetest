package com.example.machinetest.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.machinetest.entity.BranchManager;
import com.example.machinetest.exceptionhandle.BranchManagerNotFound;
import com.example.machinetest.exceptionhandle.NoDataFoundException;
import com.example.machinetest.repository.BranchManagerInterfaceServie;
import com.example.machinetest.repository.BranchManagerRepo;

@Service
public class BranchManagerService implements BranchManagerInterfaceServie{
	
	@Autowired
	BranchManagerRepo brRepo;

	@Override
	public BranchManager findById(int id) {
		BranchManager brm=brRepo.findOne(id);
		if(brm==null) {
			throw new BranchManagerNotFound(id);
		}else {
			return brm;
		}
	}

	@Override
	public BranchManager save(BranchManager branchMgr) {
		return brRepo.save(branchMgr);
	}

	@Override
	public BranchManager update(BranchManager branchMgr) {
		return brRepo.save(branchMgr);
	}

	@Override
	public void delete(int id) {
		BranchManager brm=brRepo.findOne(id);
		if(brm==null||brm.getBranchId()==null) {
			throw new BranchManagerNotFound(id);
		}else {
			if(brm!=null&&brm.getId()>0) {
				brRepo.delete(brm.getId());
			}
		}
	}

	@Override
	public List<BranchManager> findAll() {
		List<BranchManager> branchMgrList = brRepo.findAll();

        if (branchMgrList.isEmpty()) {

            throw new NoDataFoundException();
        }

        return branchMgrList;
	}

	@Override
	public BranchManager findLast() {
		List<BranchManager> branchMgrList = brRepo.findAll();
		if(branchMgrList!=null&&!branchMgrList.isEmpty()) {
			return branchMgrList.get(branchMgrList.size()-1);
		}else {
			return null;
		}
	}

	@Override
	public BranchManager findBranchManagerByToken(String token) {
		List<BranchManager> branchList=brRepo.findBranchManagerById(token);
		if(branchList!=null&&!branchList.isEmpty()){
			return branchList.get(0);
		}else {
			return null;
		}
	}

	

}
