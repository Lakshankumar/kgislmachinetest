package com.example.machinetest.exceptionhandle;

public class PolicyHolderNotFound extends RuntimeException{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PolicyHolderNotFound(int id) {

        super(String.format("Policy holder with Id %d not found", id));
    }

}
