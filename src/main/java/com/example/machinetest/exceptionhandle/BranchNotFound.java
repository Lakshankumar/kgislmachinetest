package com.example.machinetest.exceptionhandle;

public class BranchNotFound extends RuntimeException{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BranchNotFound(int id) {

        super(String.format("Branch with Id %d not found", id));
    }
}
