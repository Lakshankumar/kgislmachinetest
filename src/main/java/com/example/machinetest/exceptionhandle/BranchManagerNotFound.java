package com.example.machinetest.exceptionhandle;

public class BranchManagerNotFound extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BranchManagerNotFound(int id) {

        super(String.format("Branch Manager with Id %d not found", id));
    }

}
