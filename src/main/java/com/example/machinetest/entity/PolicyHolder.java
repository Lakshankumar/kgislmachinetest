package com.example.machinetest.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Table(name="policy_holder")
@Entity
public class PolicyHolder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "holder_token")
	private String holderToken;
	
	@Column(name = "holder_address")
	@NotNull(message = "Policy Holder can't be blank")
	private String holderAddress;
	
	@Column(name = "holder_phonenumber")
	@NotNull(message = "Phone number is required")
    @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
             message="Mobile number is invalid")
	private String holderPhonenumber;
	
	@Column(name = "holder_name")
	@NotNull(message = "Manager Name can't be blank")
	private String holderName;
	
	@Column(name = "holder_policydetails")
	@NotNull(message = "Policy details can't be blank")
	private String holderPolicyDetails;
	
	@ManyToOne
	@JoinColumn(name = "holder_link_branch")
	private BranchManager branchMgnrId;
	
	@Transient
	private String branchManagerMap;
	
	
	public PolicyHolder() {
		
	}
	
	public PolicyHolder(int id, String holderToken, String holderAddress, String holderPhonenumber, String holderName,
			String holderPolicyDetails, BranchManager branchMgnrId) {
		super();
		this.id = id;
		this.holderToken = holderToken;
		this.holderAddress = holderAddress;
		this.holderPhonenumber = holderPhonenumber;
		this.holderName = holderName;
		this.holderPolicyDetails = holderPolicyDetails;
		this.branchMgnrId = branchMgnrId;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHolderToken() {
		return holderToken;
	}

	public void setHolderToken(String holderToken) {
		this.holderToken = holderToken;
	}

	public String getHolderAddress() {
		return holderAddress;
	}

	public void setHolderAddress(String holderAddress) {
		this.holderAddress = holderAddress;
	}

	public String getHolderPhonenumber() {
		return holderPhonenumber;
	}

	public void setHolderPhonenumber(String holderPhonenumber) {
		this.holderPhonenumber = holderPhonenumber;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getHolderPolicyDetails() {
		return holderPolicyDetails;
	}

	public void setHolderPolicyDetails(String holderPolicyDetails) {
		this.holderPolicyDetails = holderPolicyDetails;
	}

	public BranchManager getBranchMgnrId() {
		return branchMgnrId;
	}

	public void setBranchMgnrId(BranchManager branchMgnrId) {
		this.branchMgnrId = branchMgnrId;
	}

	public String getBranchManagerMap() {
		return branchManagerMap;
	}

	public void setBranchManagerMap(String branchManagerMap) {
		this.branchManagerMap = branchManagerMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((holderToken == null) ? 0 : holderToken.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PolicyHolder other = (PolicyHolder) obj;
		if (holderToken == null) {
			if (other.holderToken != null)
				return false;
		} else if (!holderToken.equals(other.holderToken))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PolicyHolder [id=" + id + ", holderToken=" + holderToken + ", holderAddress=" + holderAddress
				+ ", holderPhonenumber=" + holderPhonenumber + ", holderName=" + holderName + ", holderPolicyDetails="
				+ holderPolicyDetails + ", branchMgnrId=" + branchMgnrId + "]";
	}
	
	
	
	

}
