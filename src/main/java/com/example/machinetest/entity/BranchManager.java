package com.example.machinetest.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Table(name="branch_manager")
@Entity
public class BranchManager  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "branch_mgr_token")
	private String managerToken;
	
	@Column(name = "branch_mgr_name")
	@NotNull(message = "Manager Name can't be blank")
	private String managerName;
	
	@Column(name = "branch_mgr_address")
	@NotNull(message = "Manager Address can't be blank")
	private String managerAddress;
	
	@Column(name = "branch_mgr_password")
	private String managerPassword;
	
	@Column(name = "branch_mgr_phoneno")
	@NotNull(message = "Phone number is required")
    @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
             message="Mobile number is invalid")
	private String managerPhoneNumber;
	
	@Transient
	private String branchMap;
	
	@ManyToOne
	@JoinColumn(name = "branch_link_id")
	private Branch branchId;
	
	@OneToMany(mappedBy = "branchMgnrId")
	@JsonIgnore
	private List<PolicyHolder> holderList;
	
	public BranchManager() {
		
	}
	
	

	public BranchManager(int id, String managerToken, String managerName, String managerAddress, String managerPassword,
			String managerPhoneNumber, Branch branchId) {
		super();
		this.id = id;
		this.managerToken = managerToken;
		this.managerName = managerName;
		this.managerAddress = managerAddress;
		this.managerPassword = managerPassword;
		this.managerPhoneNumber = managerPhoneNumber;
		this.branchId = branchId;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getManagerToken() {
		return managerToken;
	}

	public void setManagerToken(String managerToken) {
		this.managerToken = managerToken;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getManagerAddress() {
		return managerAddress;
	}

	public void setManagerAddress(String managerAddress) {
		this.managerAddress = managerAddress;
	}

	public String getManagerPassword() {
		return managerPassword;
	}

	public void setManagerPassword(String managerPassword) {
		this.managerPassword = managerPassword;
	}

	public String getManagerPhoneNumber() {
		return managerPhoneNumber;
	}

	public void setManagerPhoneNumber(String managerPhoneNumber) {
		this.managerPhoneNumber = managerPhoneNumber;
	}

	public Branch getBranchId() {
		return branchId;
	}

	public void setBranchId(Branch branchId) {
		this.branchId = branchId;
	}

	public List<PolicyHolder> getHolderList() {
		return holderList;
	}

	public void setHolderList(List<PolicyHolder> holderList) {
		this.holderList = holderList;
	}

	public String getBranchMap() {
		return branchMap;
	}

	public void setBranchMap(String branchMap) {
		this.branchMap = branchMap;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((managerToken == null) ? 0 : managerToken.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BranchManager other = (BranchManager) obj;
		if (id != other.id)
			return false;
		if (managerToken == null) {
			if (other.managerToken != null)
				return false;
		} else if (!managerToken.equals(other.managerToken))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BranchManager [id=" + id + ", managerToken=" + managerToken + ", managerName=" + managerName
				+ ", managerAddress=" + managerAddress + ", managerPassword=" + managerPassword
				+ ", managerPhoneNumber=" + managerPhoneNumber + ", branchId=" + branchId + "]";
	}
	
	
	

}
