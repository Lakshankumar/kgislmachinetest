package com.example.machinetest.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Table(name="branch")
@Entity
public class Branch implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "branch_token")
	private String branchToken;
	
	@Column(name = "branch_name")
	@NotNull(message = "Branch Name can't be blank")
	private String branchName;
	
	@Column(name = "branch_address")
	@NotNull(message = "Branch Address can't be blank")
	private String branchAddres;
	
	@Column(name = "branch_phonenumber")
	@NotNull(message = "Phone number is required")
    @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
             message="Mobile number is invalid")
	private String branchPhoneNumber;
	
	@OneToMany(mappedBy = "branchId")
	@JsonIgnore
	private List<BranchManager> branchMgnrList;
	
	
	public Branch() {
		
	}
	

	public Branch(int id, String branchToken, String branchName, String branchAddres, String branchPhoneNumber) {
		super();
		this.id = id;
		this.branchToken = branchToken;
		this.branchName = branchName;
		this.branchAddres = branchAddres;
		this.branchPhoneNumber = branchPhoneNumber;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBranchToken() {
		return branchToken;
	}

	public void setBranchToken(String branchToken) {
		this.branchToken = branchToken;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchAddres() {
		return branchAddres;
	}

	public void setBranchAddres(String branchAddres) {
		this.branchAddres = branchAddres;
	}

	public String getBranchPhoneNumber() {
		return branchPhoneNumber;
	}

	public void setBranchPhoneNumber(String branchPhoneNumber) {
		this.branchPhoneNumber = branchPhoneNumber;
	}
	

	public List<BranchManager> getBranchMgnrList() {
		return branchMgnrList;
	}


	public void setBranchMgnrList(List<BranchManager> branchMgnrList) {
		this.branchMgnrList = branchMgnrList;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branchToken == null) ? 0 : branchToken.hashCode());
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		if (branchToken == null) {
			if (other.branchToken != null)
				return false;
		} else if (!branchToken.equals(other.branchToken))
			return false;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	

}
